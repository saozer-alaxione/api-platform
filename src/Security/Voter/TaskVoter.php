<?php

namespace App\Security\Voter;

use App\Entity\Task;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

class TaskVoter extends Voter
{
    public const EDIT = 'CAN_EDIT_TASK';

    public function __construct(private Security $security)
    {
        
    }
    protected function supports(string $attribute, mixed $subject): bool
    {
        return in_array($attribute, [self::EDIT])
            && $subject instanceof \App\Entity\Task;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        return match($attribute) {
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!')
        };
    }

    private function canEdit(Task $task, User $user): bool
    {
        return ($this->security->isGranted('IS_AUTHENTICATED_FULLY') && ($user === $task->getUser() || $this->security->isGranted('ROLE_ADMIN')));
    }
}
