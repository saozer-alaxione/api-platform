<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\TaskRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use App\Controller\Api\TaskController;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use App\Controller\Api\GetMyTasksController;
use Symfony\Component\Serializer\Annotation\Groups;


#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: "/tasks",
            name: 'api.tasks.all',
            description: "get all tasks",
            security:"is_granted('IS_AUTHENTICATED_FULLY') and is_granted('ROLE_ADMIN')",
            normalizationContext: [
                'groups' => ['task:view']
            ]
        ),
        new GetCollection(
            uriTemplate: "/tasks/my",
            description: "Get all my tasks",
            security:"is_granted('IS_AUTHENTICATED_FULLY')",
            controller: GetMyTasksController::class
        ),
        new Get(
            uriTemplate: "/tasks/{id}",
            requirements: ['id' => '\d+'],
            name: 'api.tasks.view_one',
            description: "View a single task",
            securityPostDenormalize :"is_granted('IS_AUTHENTICATED_FULLY') and object.getUser() == user",
            normalizationContext: [
                'groups' => ['task:view']
            ]
        ),
        new Post(
            security:"is_granted('IS_AUTHENTICATED_FULLY')",
            uriTemplate: "/tasks/new",
            name: 'api.tasks.new',
            controller: TaskController::class
        ),
        new Patch(
            uriTemplate: "/tasks/{id}",
            requirements: ['id' => '\d+'],
            name: 'api.tasks.edit',
            description: "Edit a single task",
            security:"is_granted('CAN_EDIT_TASK', object)",
            denormalizationContext: [
                "group" => ['task:edit']
            ],
            normalizationContext: [
                "group" => ['task:view']
            ]
        ),
        new Delete(
            uriTemplate: "/tasks/{id}",
            requirements: ['id' => '\d+'],
            name: 'api.tasks.remove',
            description: "delete a single task",
            security:"is_granted('CAN_EDIT_TASK', object)",
            denormalizationContext: [
                "group" => ['task:edit']
            ],
            normalizationContext: [
                "group" => ['task:view']
            ]
        )
    ]
)]
#[ORM\Entity(repositoryClass: TaskRepository::class)]
class Task
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['task:view'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['task:view', 'task:edit'])]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['task:view', 'task:edit'])]
    private ?string $description = null;

    #[ORM\ManyToOne(inversedBy: 'tasks')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['task:view'])]
    private ?User $user = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
