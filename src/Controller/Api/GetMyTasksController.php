<?php

namespace App\Controller\Api;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;

class GetMyTasksController extends AbstractController
{
    public function __construct(private Security $security, private TaskRepository $taskRepository)
    {
        
    }

    public function __invoke()
    {
        $user = $this->security->getUser();
        $tasks = $this->taskRepository->findBy(['user' => $user]);
        return $tasks;
    }
}
