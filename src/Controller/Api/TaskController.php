<?php

namespace App\Controller\Api;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;

class TaskController extends AbstractController
{
    public function __construct(private Security $security, private EntityManagerInterface $entityManager)
    {
        
    }

    public function __invoke(Task $data)
    {
        $data->setUser($this->security->getUser());
        return $data;
    }
}
