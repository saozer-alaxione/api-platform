<?php

namespace App\Controller\Auth;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;

class AuthController extends AbstractController
{

    #[Route('/api/login', name: 'api.login', methods:['POST'])]
    public function login(Request $request, UserPasswordHasherInterface $passwordHasher, UserRepository $repository, AuthenticationSuccessHandler $authenticationSuccessHandler): JsonResponse
    {
        $data = $request->getPayload();
        $_username = $data->get('email')?? null;
        $_password = $data->get('password')?? null;

        if($_username != null){
            $user = $repository->findOneBy(['email' => $_username]);

            if($user && $passwordHasher->isPasswordValid($user, $_password)){

                $passwordHasher->isPasswordValid($user, $_password);
                return $authenticationSuccessHandler->handleAuthenticationSuccess($user);
            }
        }

        return $this->json([
            'code' => JsonResponse::HTTP_UNAUTHORIZED,
            'message' => 'Unathorized',
            'error' => 'Credentials missed match'
        ], JsonResponse::HTTP_UNAUTHORIZED);
    }

    /* #[Route('/api/login_verify', name: 'api.login_verify', methods:['POST'])]
    public function login_verify(#[CurrentUser] ?User $user): JsonResponse
    {
        if (null === $user) {
            return $this->json([
                'code' => JsonResponse::HTTP_UNAUTHORIZED,
                'message' => 'missing credentials',
            ], JsonResponse::HTTP_UNAUTHORIZED);
        }

        return $this->json([
            'user'  => $user->getUserIdentifier()
        ]);
    } */

}
