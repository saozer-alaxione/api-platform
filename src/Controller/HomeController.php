<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): JsonResponse
    {
        return $this->json([
            'code' => JsonResponse::HTTP_OK,
            'message' => 'Success',
            'app' => [
                'name' => 'api-platform',
                'version' => '1.0.0',
                'team' => 'alaxione-dev',
                'dev' => [
                    'alias' => 'King Saozer', 
                    'name' => 'Michel Ekanga',
                    'email' => 'michelmagloireekanga@gmail.com'
                ],
                'security' => 'Javascript Web Token (JWT)'
            ],
        ], JsonResponse::HTTP_OK);
    }
}
