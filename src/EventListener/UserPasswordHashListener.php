<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserPasswordHashListener
{

    public function __construct(private UserPasswordHasherInterface $passwordEncoder)
    {

    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {
            return;
        }

        $this->encodePassword($entity);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof User) {
            return;
        }

        $this->encodePassword($entity);
    }

    private function encodePassword(User $entity)
    {
        if (!$entity->getPassword()) {
            return;
        }

        $encoded = $this->passwordEncoder->hashPassword(
            $entity,
            $entity->getPassword()
        );

        $entity->setPassword($encoded);
        $entity->eraseCredentials();
    }
}
