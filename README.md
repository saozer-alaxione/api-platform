# GETTING STARTED WITH THE PROJECT

## System requirement

As PHP developer, you should know that, when there is a `composer.json` file, it means that you must install packages. Make sure that composer is installer in your system and type the following commands.
```bash
composer install
``` 

### Enabling TLS

Browsing the secure version of your applications locally is important to detect problems with mixed content early, and to run libraries that only run in HTTPS. Traditionally this has been painful and complicated to set up, but the Symfony server automates everything. First, run this command:

```bash
symfony server:ca:install
```

This project is made to use JWT as token authenticator. In order to use it as well, you must install OpenSSL in you local machine. OpenSSL is the key provider for `lexik/jwt-authentication-bundle`.

### Install OpenSSL for windows

Run MS-powershell `As Administrator` then type the following command.
```bash
choco install openssl
```

### Install OpenSSL for Debian/Ubuntu

```bash
sudo apt update && sudo apt install -y openssl
```

After you have installed OpenSSL, you should verify if php-extension is enabled in the `php.ini` file of your system.
### Generate keys RSA private key and public key

In the project directory, follow the steps bellow:

To generate private key:

```bash
mkdir -p config/jwt
openssl genrsa -out config/jwt/private.pem
```

and to generate the public key:
```bash
openssl rsa -in config/jwt/private.pem -pubout > config/jwt/public.pem
```

let's make sure that symfony has access to the OpenSSL process

```bash
php bin/console lexik:jwt:generate-keypair --skip-if-exists
#
# // Your key files already exist, they won't be overriden.
#
```
If you have the same ouput, that means symfony will be able to supply the JWT token easely. 

### Setup the database

create a new database and provide the connection string in the appropriate environment file. juste look the example in the default `.env` file.

Now that everything is well done. Let just migrate the database en test our application

```bash
# migrate the database
php bin/console doctrine:migrations:migrate
# run the server
symfony serve
```
Now download the Postman config file and import it in order to test the code.